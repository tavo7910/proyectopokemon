//
//  TAVPokemonDataSource.m
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/20/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVPokemonDataSource.h"

#import "TAVpokemonAPI.h"
#import "TAVimage.h"

@interface TAVPokemonDataSource()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) TAVpokemonAPI *pokemonAPI;
@property (strong, nonatomic) TAVimage *imageHelper;
@property (strong, nonatomic) NSDictionary *data;
@property (nonatomic, readonly) NSArray *titles;
@end

NSString * const TAVPokemonDataSourceCellIdentifier = @"TAVPokemonDataSourceCellIdentifier";


@implementation TAVPokemonDataSource

#pragma mark - Getters

- (TAVpokemonAPI *)pokemonAPI {
    if (!_pokemonAPI) {
        _pokemonAPI = [TAVpokemonAPI new];
    }
    return _pokemonAPI;
}

- (NSDictionary *)data {
    if (!_data) {
        _data = [NSDictionary new];
        [self fetchData];
    }
    return _data;
}

- (NSArray *)titles {
    return [self.data allKeys];
}

- (TAVimage *)imageHelper {
    if (!_imageHelper) {
        _imageHelper = [TAVimage new];
    }
    return _imageHelper;
}

#pragma mark - Fetch

- (void)fetchData {
    __weak typeof (self) weakSelf = self;
    [self.pokemonAPI getListOfPokemon:^(NSDictionary *dictionary) {
        weakSelf.data = dictionary;
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.titles.count;
}


- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.titles[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *keyNamePokemonArray = self.data[self.titles[section]];
    return keyNamePokemonArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TAVPokemonDataSourceCellIdentifier forIndexPath:indexPath];
    NSArray *keyNamePokemonArray = self.data[self.titles[indexPath.section]];
    NSDictionary *dicAux = [keyNamePokemonArray objectAtIndex:indexPath.row];
    cell.imageView.image = nil;
    cell.textLabel.text = [dicAux objectForKey:@"name"];
    cell.detailTextLabel.text = [dicAux objectForKey:@"definition"];

    NSURL *url = [NSURL URLWithString:[dicAux objectForKey:@"url"]];
    [self.imageHelper imageFromURL:url completion:^(UIImage *image) {
        cell.imageView.image = image;
        [cell setNeedsLayout];
    }];
    return cell;
    
}

@end
