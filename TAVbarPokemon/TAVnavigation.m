//
//  TAVnavigation.m
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/20/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVnavigation.h"

#import "TAVmainSplitViewController.h"

@interface TAVnavigation ()
@property (strong, nonatomic) TAVmainSplitViewController *mainSplitViewControllerDelegate;
@end

@implementation TAVnavigation

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;

    static id sharedInstance;
    dispatch_once(&once, ^ {
        sharedInstance = [self new];
        
    });
    return sharedInstance;
}

#pragma mark - Getters

- (TAVmainSplitViewController *)mainSplitViewControllerDelegate {
    if (!_mainSplitViewControllerDelegate) {
        _mainSplitViewControllerDelegate = [TAVmainSplitViewController new];
    }
    return _mainSplitViewControllerDelegate;
}

#pragma mark - setUp

- (void)setUpNavigationWithWindow:(UIWindow *)window {
    UISplitViewController *splitViewController = (UISplitViewController *)window.rootViewController;
    UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
    navigationController.topViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem;
    splitViewController.delegate = self.mainSplitViewControllerDelegate;
}

@end
