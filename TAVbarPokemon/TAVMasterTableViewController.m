//
//  TAVMasterTableViewController.m
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/20/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVMasterTableViewController.h"
#import "TAVDetailViewController.h"
#import "TAVPokemonDataSource.h"

@interface TAVMasterTableViewController()

@end
@implementation TAVMasterTableViewController

#pragma mark - View Controller

- (void)viewWillAppear:(BOOL)animated {
    // investigar que putas es eto.
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        
        
        //Aqui capturo el indice de la tabla seleccionada
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        //aqui obtengo la celda seleccionada de la tabla y se la asigno a una variable tipo celda
        //porque me es mas facil usarla asi despues en vez de tener una gran linea de codigo para obtener
        //lo que necesito
        UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:indexPath];
        //UIImage *imageBack;
        TAVDetailViewController *controller = (TAVDetailViewController *)[[segue destinationViewController] topViewController];
        
        UIImage *pokemonPhoto = selectedCell.imageView.image;
        
        
        [controller setDetailItem:selectedCell.textLabel.text withFrontPhoto:pokemonPhoto withPokemonNumber:((indexPath.row)+1)];
        
        [controller setImagaSelected:selectedCell.imageView.image];
        
        
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Actions

- (IBAction)reloadDataAction:(id)sender {
    [self.tableView reloadData];
}

@end
