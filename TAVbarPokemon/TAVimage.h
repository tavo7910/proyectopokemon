//
//  TAVimage.h
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/20/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

@import UIKit;

@interface TAVimage : NSObject
- (void)imageFromURL:(NSURL *)url completion:(void (^)(UIImage *image))completion;

@end
