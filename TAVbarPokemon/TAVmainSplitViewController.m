//
//  TAVmainSplitViewController.m
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/20/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVmainSplitViewController.h"
#import "TAVDetailViewController.h"

@implementation TAVmainSplitViewController

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    
    BOOL navigationControllerInSecondaryViewController                  = [secondaryViewController isKindOfClass:[UINavigationController class]];
    BOOL detailViewControllerIsPresentedBySecondaryNavigationController = [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[TAVDetailViewController class]];
    BOOL selectedPokemonExistInDetailViewController                   = !((TAVDetailViewController *)[(UINavigationController *)secondaryViewController topViewController]).selectedPokemon;
    
    if (navigationControllerInSecondaryViewController && detailViewControllerIsPresentedBySecondaryNavigationController && selectedPokemonExistInDetailViewController) {
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
    }
    else {
        return NO;
    }
}


@end
