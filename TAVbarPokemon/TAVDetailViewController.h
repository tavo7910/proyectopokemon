//
//  TAVDetailViewController.h
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/20/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAVDetailViewController : UIViewController

@property (strong, nonatomic) NSString *selectedPokemon;
@property (strong, nonatomic) UIImage *imagaSelected;

- (void)setDetailItem:(NSString *)newDetailItem withFrontPhoto:(UIImage *)pokemonPhoto withPokemonNumber:(NSInteger)pokemonNumber;
- (void)setImageToDetail:(UIImage *)newImage;

@end
