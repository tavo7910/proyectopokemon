//
//  AppDelegate.h
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/19/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

