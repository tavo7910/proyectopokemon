//
//  TAVpokemonAPI.m
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/20/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVpokemonAPI.h"


NSString *const APIURL = @"https://api.myjson.com/bins/klvnv";
//NSInteger AUX=1;

@interface TAVpokemonAPI ()

@end

@implementation TAVpokemonAPI

- (void)getListOfPokemon :(void (^)(NSDictionary *dictionary))completionBlock {
//    if(AUX==1){
//    APIURL = @"https://api.myjson.com/bins/klvnv";
//        AUX=0;
//    } else {
//    APIURL = @"https://api.myjson.com/bins/gm9wr";
//        AUX = 1;
//    }
    NSURL *url = [NSURL URLWithString:APIURL];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        id result = nil;
        if (!error) {
            NSError *JSONError = nil;
            result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&JSONError];
            if (![result isKindOfClass:[NSDictionary class]] || JSONError) {
                result = nil;
            }
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionBlock) {
                completionBlock(result);
            }
        });
    }];
    
    [task resume];

}
@end
