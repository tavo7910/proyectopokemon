//
//  main.m
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/19/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
