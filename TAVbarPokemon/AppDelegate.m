//
//  AppDelegate.m
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/19/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "AppDelegate.h"
#import "TAVnavigation.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[TAVnavigation sharedInstance] setUpNavigationWithWindow:self.window];
    return YES;
}
@end
