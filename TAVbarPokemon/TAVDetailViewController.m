//
//  TAVDetailViewController.m
//  TAVbarPokemon
//
//  Created by tavo7910 on 3/20/17.
//  Copyright © 2017 tavo7910. All rights reserved.
//

#import "TAVDetailViewController.h"
#import "TAVMasterTableViewController.h"
#import "TAVBerriDataSource.h"
#import "TAVimage.h"


@interface TAVDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *definitionType;
@property (weak, nonatomic) IBOutlet UIImageView *viewPokemon;
@property (strong, nonatomic) IBOutlet UIImageView *viewBackPokemon;
@property (strong, nonatomic) UIImage *pokemonPhoto;
@property (nonatomic) NSInteger pokemonNumber;
@property (strong, nonatomic) TAVimage *imageHelper;
@end

NSString *const urlBackString = @"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/";
NSString *const pngFinal = @".png";

@implementation TAVDetailViewController

- (void)configureView {
    if (self.selectedPokemon) {
        self.definitionType.text = self.selectedPokemon;
        NSString *completeURL =[NSString stringWithFormat:@"%@%lu%@",urlBackString,self.pokemonNumber,pngFinal];
        NSURL *urlBackComplete = [NSURL URLWithString:completeURL];
        [self.imageHelper imageFromURL:urlBackComplete completion:^(UIImage *image) {
            self.viewBackPokemon.image = image;
        }];
        
    }
}

- (void)configureImage{
    if(self.imagaSelected){
        self.viewPokemon.image = self.imagaSelected;
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
    [self configureImage];
}

- (TAVimage *)imageHelper {
    if (!_imageHelper) {
        _imageHelper = [TAVimage new];
    }
    return _imageHelper;
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(NSString *)newDetailItem withFrontPhoto:(UIImage *)pokemonPhoto withPokemonNumber:(NSInteger)pokemonNumber{
    if (![_selectedPokemon isEqualToString:newDetailItem]) {
        _selectedPokemon = newDetailItem;
        self.pokemonPhoto = pokemonPhoto;
        self.pokemonNumber = pokemonNumber;
        [self configureView];
    }
}

- (void)setImageToDetail:(UIImage *)newImage {
    if(![_imagaSelected isEqual:newImage]){
        _imagaSelected = newImage;
        [self configureImage];
    }
}

@end
